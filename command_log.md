# Command Log

Yum is not working out of the box. All mirrors are returning 404 - Not found. To remedy this:

```bash
sudo echo 'http_caching=packages' >> /etc/yum.conf
```

Now only RPM package downloads will be cached by yum (but not repository metadata downloads). Now we can update yum:

```bash
sudo yum update
```

## Drupal Pre-requisites

#### Apache

```bash
sudo yum install httpd
sudo systemctl enable httpd
sudo systemctl start httpd
```

Edit `/etc/httpd/conf/httpd.conf`:

```bash
sudo vim /etc/httpd/conf/httpd.conf
```

The `/var/www/html` directory options should look like this:

```conf
<Directory "/var/www/html">
    #
    # Possible values for the Options directive are "None", "All",
    # or any combination of:
    #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
    #
    # Note that "MultiViews" must be named *explicitly* --- "Options All"
    # doesn't give it to you.
    #
    # The Options directive is both complicated and important.  Please see
    # http://httpd.apache.org/docs/2.4/mod/core.html#options
    # for more information.
    #
    Options Indexes FollowSymLinks

    #
    # AllowOverride controls what directives may be placed in .htaccess files.
    # It can be "All", "None", or any combination of the keywords:
    #   Options FileInfo AuthConfig Limit
    #
    AllowOverride All

    #
    # Controls who can get stuff from this server.
    #
    Require all granted
</Directory>
```

After you have saved your changes, restart Apache:

```bash
sudo systemctl restart httpd
```

#### PostgreSQL

```bash
# Install the official PostgreSQL yum repo
sudo yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
# Newest PostgreSQL
sudo yum install postgres12
sudo yum install postgres12-server
# Initialize the db
sudo /usr/pgsql-12/bin/postgresql-12-setup initdb
sudo systemctl enable postgresql-12
# Start the db server
sudo systemctl start postgresql-12
```

#### PHP 7

```bash
# Add epel repo and install yum-utils -- yum-utils includes yum-config-manager, which we will use below
sudo yum install epel-release yum-utils
sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
# Enable the Remi repo
sudo yum-config-manager --enable remi-php73
# Install php and some common modules
sudo yum install php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-pgsql php-xml php-mbstring
```

## Drupal

#### Drush

```bash
# Get latest drush.phar for Drush 8.x
wget https://github.com/drush-ops/drush/releases/download/8.3.2/drush.phar
# Test Drush install
php drush.phar core-status
# Rename and move Drush
chmod 755 drush.phar
sudo cp drush.phar /usr/local/bin/drush
# Delete drush.phar copy
rm drush.phar
# Enrich bash startup with completion and aliases
drush init
```

Now exit and re-enter ssh to experience drush improvements.

#### Download Drupal

```bash
cd /var/www/html
# Download Drupal
drush dl drupal-7
# Move files into /var/www/html
mv drupal-7.69/* .
mv drupal-7.69/.editorconfig .
mv drupal-7.69/.gitignore .
mv drupal-7.69/.htaccess .
# Remove now empty Drupal repo
rm -d drupal-7.69
# Change permissions, owner, and group
sudo chmod -R g+w .
sudo chown -R apache .
sudo chgrp -R richterlab .
```

#### Create the db

```bash
# Change user to postgres
sudo su postgres
# Create drupal db user
createuser --pwprompt --encrypted --no-adduser --no-createdb drupal
# Create drupal db
createdb --encoding=UNICODE --owner=drupal drupal
# Change the postgres configuration so that the drupal user can access the db
vim /var/lib/pgsql/12/data/pg_hba.conf
```

The bottom few lines of this file should look like this:

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                peer

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```

Now exit postgres user and restart postgres

```bash
exit
sudo systemctl restart postgresql-12.service
# Test configuration works
psql -d drupal -U drupal -h 127.0.0.1
```

#### Install Drupal

```bash
# Add site configuration file with ServerName
sudo echo 'ServerName obgpdb.org' > /etc/httpd/conf.d/obgpdb.org.conf
# Restart Apache
sudo systemctl restart httpd
```

Now navigate to `http://<site url>/install.php` in the browser and follow the installation instructions.

## Tripal

#### Tripal Pre-requisites

```bash
cd /var/www/html/sites/all/modules
drush en entity
drush en views viws_ui
drush en ctools
drush en ds field_group field_group_table field_formatter_class field_formatter_settings
drush en ckeditor
drush en jquery_update
```

#### Tripal

We're finally ready to install Tripal!

```bash
cd /var/www/html/sites/all/modules
git clone https://github.com/tripal/tripal.git
drush en tripal
drush en tripal_chado
drush en tripal_ds tripal_ws
```

Now we need to install chado. Navigate to `http://<site url>/admin/tripal/storage/chado/install` and select "New Install of Chado v1.3 (erases all existing Chado data if Chado already exists)", then click "Install/Upgrade Chado".

This will submit a Tripal Job to install chado. The job can be run via the Tripal Jobs system.

```bash
drush trp-run-jobs --username=admin
```

The job should now appear complete on the jobs page (`http://<site url>/admin/tripal/tripal_jobs`).

Now we need to prepare both Drupal and Chado. Navigate to `http://<site url>/admin/tripal/storage/chado/prepare` and click "Prepare this site", then run the Tripal Jobs system again.

```bash
drush trp-run-jobs --username=admin
```

All of the jobs on the jobs page should now appear complete. Tripal has now been successfully installed!

## tripal_jbrowse

Now we want to install [tripal_jbrowse](https://github.com/tripal/tripal_jbrowse).

#### JBrowse

```bash
# JBrowse Pre-requisites
sudo yum groupinstall "Development Tools"
sudo yum install zlib-devel perl-ExtUtils-MakeMaker
cd /var/www/html
mkdir jbrowse
cd jbrowse
# Download JBrowse
wget https://github.com/GMOD/jbrowse/releases/download/1.16.8-release/JBrowse-1.16.8.zip
unzip JBrowse-1.16.8.zip
rm JBrowse-1.16.8.zip
mv JBrowse-1.16.8/* .
rm -d JBrowse-1.16.8/
# Setup JBrowse
./setup.sh
# Create directory for JBrowse data to live in
mkdir data
# Set permissions
sudo chown -R apache .
sudo chgrp -R richterlab .
sudo chmod -R g+w .
```

This `setup.sh` script can take a long time to run, but will eventually finish.

#### tripal_jbrowse

```bash
cd /var/www/html/sites/all/modules
git clone https://github.com/tripal/tripal_jbrowse.git
drush en tripal_jbrowse
```

Navigate to `http://<site url>/admin/modules` and enable the "Tripal JBrowse Management" module.

Now navigate to `http://<site url>/admin/tripal/extension/tripal_jbrowse/management/configure` and configure tripal_jbrowse.

Now go back to `http://<site url>/admin/modules` and enable the "Tripal-JBrowse Page Integration" module.

New JBrowse instances can now be created at `http://<site url>/admin/tripal/extension/tripal_jbrowse/management`.
